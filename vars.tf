variable "AWS_ACCESS_KEY" {
  type = string
}

variable "AWS_SECRET_KEY" {
  type = string
}

variable "AWS_REGION" {
  default = "us-east-1"
}

variable "AMIS" {
  type = map(string)
  default = {
    "us-east-1" = "ami-0c2b8ca1dad447f8a"
    "us-east-2" = "ami-0443305dabd4be2bc"
    "us-west-1" = "ami-04b6c97b14c54de18"
    "us-west-2" = "ami-083ac7c7ecf9bb9b0"
  }


}
variable "PATH_TO_PRIVATE_KEY" {
  default = "acg_kpmint"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "acg_kpmint.pub"
}

variable "SECURITY_GROUP" {
  type = string
}

variable "INSTANCE_USERNAME" {
  type    = string
  default = "ec2-user"
}